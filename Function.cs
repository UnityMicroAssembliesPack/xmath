﻿using UnityEngine;

public static partial class XMath
{
    public interface IFunction<T_ArgumentType> {
        T_ArgumentType get(float inParam);
    }

    public struct GravityFunction : IFunction<Vector3> {
        public GravityFunction(Vector3 inPosition, Vector3 inSpeed, float inGravityForce) {
            _position = inPosition;
            _speed = inSpeed;
            _gravityForce = inGravityForce;
        }

        public Vector3 get(float inParam) {
            float theDT = inParam;

            Vector2 theXZSpeed = getVector2From(_speed);
            Vector2 theXZ = getVector2From(_position) + theXZSpeed*theDT;

            float theY = _position.y + (_speed.y + _gravityForce*theDT/2)*theDT;

            return new Vector3(theXZ[0], theY, theXZ[1]);
        }

        private Vector3 _position;
        private Vector3 _speed;
        private float _gravityForce;
    }

    public static void get<T_FunctionType, T_ArgumentType>(this T_FunctionType inFunction,
        FastArray<T_ArgumentType> inoutValues, float inStartValue, float inValueStep, bool inIncludeStartValue = false)
        where T_FunctionType : struct, IFunction<T_ArgumentType>
    {
        XUtils.check(inoutValues);

        float theCurrentValue = inStartValue + (inIncludeStartValue ? 0.0f : inValueStep);
        int theStepsNumber = inoutValues.getSize();
        for (int theStepIndex = 0; theStepIndex < theStepsNumber; ++theStepIndex) {
            inoutValues[theStepIndex] = inFunction.get(theCurrentValue);
            theCurrentValue += inValueStep;
        }
    }
}
