﻿using UnityEngine;
using static XMath;

namespace XMathTypes
{
    // ----------------------- Vectors -------------------------

    [System.Serializable]
    public struct IntVector2 {
        public IntVector2(int inX, int inY) {
            x = inX;
            y = inY;
        }

        public IntVector2(Vector2 inVector, RoundingType inRoundingType) {
            x = roundToInt(inVector.x, inRoundingType);
            y = roundToInt(inVector.y, inRoundingType);
        }

        public static IntVector2 operator -(IntVector2 inVector) {
            return new IntVector2(-inVector.x, -inVector.y);
        }

        public static Vector2 operator *(IntVector2 inVector, float inScalar) {
            return new Vector2(inVector.x * inScalar, inVector.y * inScalar);
        }

        public static IntVector2 zero { get { return new IntVector2(0, 0); } }
        public static IntVector2 one { get { return new IntVector2(1, 1); } }

        public int x;
        public int y;
    }

    [System.Serializable]
    public struct IntVector3 {
        public IntVector3(int inX, int inY, int inZ) {
            x = inX; y = inY; z = inZ;
        }

        public IntVector3(Vector3 inVector, RoundingType inRoundingType) {
            x = roundToInt(inVector.x, inRoundingType);
            y = roundToInt(inVector.y, inRoundingType);
            z = roundToInt(inVector.z, inRoundingType);
        }

        public IntVector3(UintVector3 inVector) {
            x = safeCast(inVector.x);
            y = safeCast(inVector.y);
            z = safeCast(inVector.z);
        }

        public IntVector3 diraction {
            get { return new IntVector3(getSign(x), getSign(y), getSign(z)); }
        }

        public bool isZero() {
            return (0 == x && 0 == y && 0 == z);
        }

        public static IntVector3 operator *(IntVector3 inVector, int inScalar) {
            return new IntVector3(inVector.x * inScalar, inVector.y * inScalar, inVector.z * inScalar);
        }

        public static IntVector3 operator /(IntVector3 inVector, int inScalar) {
            return new IntVector3(inVector.x / inScalar, inVector.y / inScalar, inVector.z / inScalar);
        }

        public static Vector3 operator *(IntVector3 inVector, float inScalar) {
            return new Vector3(inVector.x * inScalar, inVector.y * inScalar, inVector.z * inScalar);
        }

        public static IntVector3 operator +(IntVector3 inVectorA, IntVector3 inVectorB) {
            return new IntVector3(inVectorA.x + inVectorB.x, inVectorA.y + inVectorB.y, inVectorA.z + inVectorB.z);
        }

        public static IntVector3 operator -(IntVector3 inVectorA, IntVector3 inVectorB) {
            return new IntVector3(inVectorA.x - inVectorB.x, inVectorA.y - inVectorB.y, inVectorA.z - inVectorB.z);
        }

        public static bool operator ==(IntVector3 inVectorA, IntVector3 inVectorB) {
            return (inVectorA.x == inVectorB.x && inVectorA.y == inVectorB.y && inVectorA.z == inVectorB.z);
        }

        public static bool operator !=(IntVector3 inVectorA, IntVector3 inVectorB) {
            return !(inVectorA == inVectorB);
        }

        public static IntVector3 operator -(IntVector3 inVector) {
            return new IntVector3(-inVector.x, -inVector.y, -inVector.z);
        }

        public static Vector3 perComponentMultiply(IntVector3 inVectorA, Vector3 inVectorB) {
            return new Vector3(inVectorA.x * inVectorB.x, inVectorA.y * inVectorB.y, inVectorA.z * inVectorB.z);
        }

        public static implicit operator Vector3(IntVector3 inVector) {
            return new Vector3(inVector.x, inVector.y, inVector.z);
        }

        public static implicit operator UintVector3(IntVector3 inVector) {
            return new UintVector3(inVector);
        }

        public int this[int theIndex] {
            get {
                switch(theIndex) {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    default: XUtils.check(false); return 0;
                }
            }
            
            set {
                switch(theIndex) {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    default: XUtils.check(false); break;
                }
            }
        }

        public override string ToString() { return "(" + x + ", " + y + ", " + z + ")"; }
        public override bool Equals(object obj) { return base.Equals(obj); }
        public override int GetHashCode() { return base.GetHashCode(); }

        public static IntVector3 zero { get { return new IntVector3(0, 0, 0); } }
        public static IntVector3 one { get { return new IntVector3(1, 1, 1); } }

        public int x;
        public int y;
        public int z;
    }

    [System.Serializable]
    public struct IntBounds3 {
        public IntBounds3(IntVector3 inOffset, UintVector3 inSize) {
            _offset = inOffset;
            _size = inSize;
        }

        public bool isPointInside(IntVector3 inPoint) {
            IntVector3 theOffsetRelatedPoint = inPoint - _offset;
            if (theOffsetRelatedPoint.x < 0 || theOffsetRelatedPoint.x >= _size.x) return false;
            if (theOffsetRelatedPoint.y < 0 || theOffsetRelatedPoint.y >= _size.y) return false;
            if (theOffsetRelatedPoint.z < 0 || theOffsetRelatedPoint.z >= _size.z) return false;
            return true;
        }

        public void expand(IntBounds3 inOtherBounds) {
            mergeToRange(ref _offset.x, ref _size.x, inOtherBounds._offset.x, inOtherBounds._size.x);
            mergeToRange(ref _offset.y, ref _size.y, inOtherBounds._offset.y, inOtherBounds._size.y);
            mergeToRange(ref _offset.z, ref _size.z, inOtherBounds._offset.z, inOtherBounds._size.z);
        }

        public void expand(IntVector3 inPoint) {
            mergeToRange(ref _offset.x, ref _size.x, inPoint.x, 0);
            mergeToRange(ref _offset.y, ref _size.y, inPoint.y, 0);
            mergeToRange(ref _offset.z, ref _size.z, inPoint.z, 0);
        }

        public IntVector3 offset {
            get { return _offset; }
            set { _offset = value; }
        }

        public UintVector3 size {
            get { return _size; }
            set { _size = value; }
        }

        public void rotate(VectorDimention inAxis, int inHalfPINum) {
            _offset = getVectorRotatedByHalfPINum(_offset, inAxis, inHalfPINum);

            IntVector3 theSignedRotatedSize = getVectorRotatedByHalfPINum(new IntVector3(_size), inAxis, inHalfPINum);
            for (int theIndex = 0; theIndex < 3; ++theIndex) {
                if (theSignedRotatedSize[theIndex] < 0) {
                    _offset[theIndex] += theSignedRotatedSize[theIndex];
                    theSignedRotatedSize[theIndex] = -theSignedRotatedSize[theIndex];
                }
            }
            _size = theSignedRotatedSize;
        }

        public static IntBounds3 identity { get { return new IntBounds3(IntVector3.zero, UintVector3.one); } }

        public override string ToString() {
            return _offset.ToString() + " | " + _size.ToString();
        }

        public IntVector3 _offset;
        public UintVector3 _size;
    }

    [System.Serializable]
    public struct ProtectedVector3 {
        public struct DimensionInfo {
            public VectorDimention dimension;
            public float value;

            public DimensionInfo(VectorDimention inDimension, float inValue) {
                dimension = inDimension;
                value = inValue;
            }
        }

        public FastArray<DimensionInfo> getDementionsSorted() {
            __reg_sortedDinesions.clear();

            if (x.isSet()) __reg_sortedDinesions.add(new DimensionInfo(VectorDimention.X, x.value));
            if (y.isSet()) __reg_sortedDinesions.add(new DimensionInfo(VectorDimention.Y, y.value));
            if (z.isSet()) __reg_sortedDinesions.add(new DimensionInfo(VectorDimention.Z, z.value));

            XUtils.sort(__reg_sortedDinesions, (DimensionInfo inA, DimensionInfo inB)=>{
                return XUtils.compare(inA.value, inB.value);
            });
            return __reg_sortedDinesions;
        }
        private static FastArray<DimensionInfo> __reg_sortedDinesions = new FastArray<DimensionInfo>();

        private Optional<DimensionInfo> getDimensionInfo(VectorDimention inDimention, Optional<float> inValue) {
            return inValue.isSet() ?
                new Optional<DimensionInfo>(new DimensionInfo(inDimention, inValue.value)) :
                new Optional<DimensionInfo>();
        }

        public Optional<float> x;
        public Optional<float> y;
        public Optional<float> z;
    }

    [System.Serializable]
    public struct UintVector3 {
        public UintVector3(uint inX, uint inY, uint inZ) {
            x = inX; y = inY; z = inZ;
        }

        public UintVector3(IntVector3 inVector) {
            x = safeCast(inVector.x);
            y = safeCast(inVector.y);
            z = safeCast(inVector.z);
        }

        public static implicit operator Vector3(UintVector3 inVector) {
            return new Vector3(inVector.x, inVector.y, inVector.z);
        }

        public static implicit operator IntVector3(UintVector3 inVector) {
            return new IntVector3(inVector);
        }

        public uint this[int theIndex] {
            get {
                switch(theIndex) {
                    case 0: return x;
                    case 1: return y;
                    case 2: return z;
                    default: XUtils.check(false); return 0;
                }
            }
            
            set {
                switch(theIndex) {
                    case 0: x = value; break;
                    case 1: y = value; break;
                    case 2: z = value; break;
                    default: XUtils.check(false); break;
                }
            }
        }

        public static UintVector3 operator *(UintVector3 inVector, uint inScalar) {
            return new UintVector3(inVector.x * inScalar, inVector.y * inScalar, inVector.z * inScalar);
        }

        public static IntVector3 operator *(UintVector3 inVector, int inScalar) {
            return new IntVector3(inVector) * inScalar;
        }

        public override string ToString() {
            return "(" + x + ", " + y + ", " + z + ")";
        }

        public uint x;
        public uint y;
        public uint z;

        public static UintVector3 zero { get { return new UintVector3(0, 0, 0); } }
        public static UintVector3 one { get { return new UintVector3(1, 1, 1); } }
    }

    // ----------------------- Segments -------------------------

    public struct Segment2D {
        public Segment2D(Vector2 inPointA, Vector2 inPointB) {
            pointA = inPointA;
            pointB = inPointB;
        }

        public Vector2 pointA { get; set; }
        public Vector2 pointB { get; set; }

        public static Optional<Vector2> getIntersection(
            Segment2D inSegamenA, Segment2D inSegamenB)
        {
            Vector2 p1 = inSegamenA.pointA;
            Vector2 p2 = inSegamenA.pointB;
            Vector2 p3 = inSegamenB.pointA;
            Vector2 p4 = inSegamenB.pointB;

            var d = (p2.x - p1.x) * (p4.y - p3.y) - (p2.y - p1.y) * (p4.x - p3.x);
            if (d == 0.0f) return new Optional<Vector2>();

            var u = ((p3.x - p1.x) * (p4.y - p3.y) - (p3.y - p1.y) * (p4.x - p3.x)) / d;
            var v = ((p3.x - p1.x) * (p2.y - p1.y) - (p3.y - p1.y) * (p2.x - p1.x)) / d;
            if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f) return new Optional<Vector2>();

            Vector2 theIntersection = new Vector2(
                p1.x + u * (p2.x - p1.x),
                p1.y + u * (p2.y - p1.y)
            );
            return new Optional<Vector2>(theIntersection);
        }
    }

    public struct Segment3D {
        public Segment3D(Vector3 inPointA, Vector3 inPointB)
        {
            pointA = inPointA;
            pointB = inPointB;
        }

        public Vector3 pointA { get; set; }
        public Vector3 pointB { get; set; }
    }

    // -------------------- Polygons -------------------------

    [System.Serializable]
    public struct ConvexPolygon2D {
        public ConvexPolygon2D(Vector2[] inVertices = null, bool inCloneArray = true) {
            bool theIsValidVertices = (null != inVertices);
            theIsValidVertices &= (inVertices.Length >= 3);

            if (theIsValidVertices) {
                _vertices = inCloneArray ? XUtils.cloneArray(inVertices) : inVertices;
                orientVerteciesByClockwise();
            } else {
                _vertices = null;
            }
        }

        public bool isValid() {
            return null != _vertices;
        }

        public Vector2[] getVertices() {
            return _vertices;
        }

        //NB: Was refactored without tests. May work incorrect
        public bool isPointInside(Vector2 inPoint) {
            XUtils.check(isValid());

            int theLength = _vertices.Length;
            Vector2 thePreviousVertex = _vertices[theLength - 1];
            for (int theVertexIndex = 0; theVertexIndex < theLength; ++theVertexIndex) {
                Vector2 theCurrentVertex = _vertices[theVertexIndex];

                Vector2 theVerticesDelta = theCurrentVertex - thePreviousVertex;
                Vector2 thePointDelta = inPoint - thePreviousVertex;
                if (!XMath.areVectorsClockwiseOriented(theVerticesDelta, thePointDelta)) return false;

                thePreviousVertex = theCurrentVertex;
            }
            return true;
        }

        public Vector2 getCenter() {
            Vector2 theResult = new Vector2();
            foreach (Vector2 theVertex in _vertices) {
                theResult += theVertex;
            }
            theResult /= _vertices.Length;

            return theResult;
        }

        public Vector2 boundMin {
            get {
                Vector2 theResult = _vertices[0];
                int theVerticiesNum = _vertices.Length;
                for (int theIndex = 1; theIndex < theVerticiesNum; ++theIndex) {
                    Vector2 theVertex = _vertices[theIndex];
                    if (theResult.x > theVertex.x) {
                        theResult.x = theVertex.x;
                    }
                    if (theResult.y > theVertex.y) {
                        theResult.y = theVertex.y;
                    }
                }
                return theResult;
            }
        }

        public Vector2 boundMax {
            get {
                Vector2 theResult = _vertices[0];
                int theVerticiesNum = _vertices.Length;
                for (int theIndex = 1; theIndex < theVerticiesNum; ++theIndex) {
                    Vector2 theVertex = _vertices[theIndex];
                    if (theResult.x < theVertex.x) {
                        theResult.x = theVertex.x;
                    }
                    if (theResult.y < theVertex.y) {
                        theResult.y = theVertex.y;
                    }
                }
                return theResult;
            }
        }

        public Vector2 getNearestConvexPoint(Vector2 inPoint) {
            XUtils.check(isValid());

            if (isPointInside(inPoint)) return inPoint;

            //Test if point is nearer to corners

            //Debug.Log("{");
            int theLength = _vertices.Length;
            Vector2 thePreviousVertex = _vertices[theLength - 1];
            for (int theVertexIndex = 0; theVertexIndex < theLength - 1; ++theVertexIndex) {
                Vector2 theCurrentVertex = _vertices[theVertexIndex];
                Vector2 theNextVertex = _vertices[theVertexIndex + 1];

                Vector2 thePointVector = inPoint - theCurrentVertex;
                Vector2 thePreviousSideNormal = XMath.getVectorRotatedByHalfPINum(thePreviousVertex - theCurrentVertex, 1);
                Vector2 theNextSideNormal = XMath.getVectorRotatedByHalfPINum(theNextVertex - theCurrentVertex, -1);

                //Debug.Log("\t" + thePointVector + "  |  " + thePreviousSideNormal + "  |  " + theNextSideNormal + "  |  " +
                //    (XMath.areVectorsClockwiseOriented(thePreviousSideNormal, thePointVector) ? "true" : "false") +
                //    (XMath.areVectorsClockwiseOriented(thePointVector, theNextSideNormal) ? "true" : "false")
                //);

                if (XMath.areVectorsClockwiseOriented(thePreviousSideNormal, thePointVector) &&
                    XMath.areVectorsClockwiseOriented(thePointVector, theNextSideNormal))
                {
                    return theCurrentVertex;
                }

                thePreviousVertex = theCurrentVertex;
            }
            //Debug.Log("}");

            //Find nearest point on sides
            float theBestDistanceSqr = float.MaxValue;
            Vector2 theNearestPoint = new Vector2();

            thePreviousVertex = _vertices[theLength - 1];
            for (int theVertexIndex = 0; theVertexIndex < theLength; ++theVertexIndex) {
                Vector2 theCurrentVertex = _vertices[theVertexIndex];
                Vector2 theVerticesDelta = theCurrentVertex - thePreviousVertex;
                Vector2 thePointDelta = inPoint - thePreviousVertex;

                Vector2 theProjectionOnSide = XMath.project(thePointDelta, theVerticesDelta);
                float theDistanceSqr = (theProjectionOnSide - thePointDelta).sqrMagnitude;

                if (theDistanceSqr < theBestDistanceSqr) {
                    if (XMath.isVectorInsideVector(theVerticesDelta, theProjectionOnSide)) {
                        theNearestPoint = thePreviousVertex + theProjectionOnSide;
                        theBestDistanceSqr = theDistanceSqr;
                    }
                }

                thePreviousVertex = theCurrentVertex;
            }
            return theNearestPoint;
        }

        public Optional<Vector2> getFirstSegmentIntersection(Segment2D inSegment) {
            Segment2D theSide = new Segment2D();

            Optional<Vector2> theIntersection;

            theSide.pointA = _vertices[0];
            int theVerticesNum = _vertices.Length;
            for (int theIndex = 0; theIndex < theVerticesNum; ++theIndex) {
                theSide.pointB = _vertices[theIndex];

                theIntersection = Segment2D.getIntersection(theSide, inSegment);
                if (theIntersection.isSet()) return theIntersection;

                theSide.pointA = theSide.pointB;
            }

            theSide.pointB = _vertices[0];
            return Segment2D.getIntersection(theSide, inSegment);
        }

        private void orientVerteciesByClockwise() {
            Vector2 theDiractionToSecondVertex = _vertices[1] - _vertices[0];
            Vector2 theDiractionToThirdVertex = _vertices[2] - _vertices[0];

            if (XMath.areVectorsClockwiseOriented(
                theDiractionToSecondVertex, theDiractionToThirdVertex)) return;

            XUtils.reverseArray(_vertices);
        }

        public Vector2[] _vertices; //NB: public only for serialization
    }

    // ------------------ Oriented plane -----------------------

    [System.Serializable]
    public struct OrientedPlane {
        public OrientedPlane(Vector3 inOriginPoint, Vector3 inNormal,
            Vector3 inXAxisOrientation)
        {
            XUtils.check(!XMath.areVectorsCooriented(
                inNormal, inXAxisOrientation
            ));

            Plane thePlane = new Plane(inNormal, inOriginPoint);
            Vector3 theXAxisOrientationPlanePoint =
                thePlane.ClosestPointOnPlane(inXAxisOrientation);
            Vector3 theZAxisOrientationPlanePoint = Vector3.Cross(
                theXAxisOrientationPlanePoint, inNormal
            );

            Quaternion theRotation = Quaternion.LookRotation(theZAxisOrientationPlanePoint, inNormal);

            _planeTransform = new LocatorTransform(inOriginPoint, theRotation);
        }

        public Vector3 originPoint {
            get { return transform.position; }
        }

        public LocatorTransform transform {
            get { return _planeTransform; }
        }

        public Vector2 getPlanePoint(Vector3 inWorldPoint) {
            Plane theUnityPlane = _planeTransform.getPlane(TransformPlane.XZ);
            Vector3 theWorldPointOnPlane = theUnityPlane.ClosestPointOnPlane(inWorldPoint);
            return XMath.getVector2From(_planeTransform.getLocalPoint(theWorldPointOnPlane), TransformPlane.XZ);
        }

        public Vector3 getWorldPoint(Vector2 inPlanePoint) {
            Vector3 thePlanePointConverted = XMath.getVector3From(inPlanePoint, TransformPlane.XZ);
            return _planeTransform.getGlobalPoint(thePlanePointConverted);
        }

        public Optional<Vector2> raycastPlanePoint(Ray inRay) {
            return getPlanePoint(raycastWorldPoint(inRay).value);
        }

        public Optional<Vector3> raycastWorldPoint(Ray inRay) {
            Plane theUnityPlane = new Plane(_planeTransform.axisY, _planeTransform.position);
            float theDistance; bool theIntersectionFound = theUnityPlane.Raycast(inRay, out theDistance);
            return new Optional<Vector3>(inRay.origin + inRay.direction * theDistance, theIntersectionFound);
        }

        public LocatorTransform getTransformFor(Vector2 inPlanePoint, float inPlaneRotation) {
            Vector3 thePosition = getWorldPoint(inPlanePoint);
            Quaternion theRotation = _planeTransform.rotation *
                Quaternion.Euler(0.0f, inPlaneRotation, 0.0f);

            return new LocatorTransform(thePosition, theRotation);
        }

        //NB: public for serializing only
        public LocatorTransform _planeTransform;
    }
}

// ========================== Change trackers ===============================

namespace XChangesTracking
{
    [System.Serializable]
    public class CT_Transform : CT_Base {
        public CT_Transform(Transform inTransformToTrack, XMathTypes.Locality inLocality = XMathTypes.Locality.Local) {
            setAsNotNull();

            _transformToTrack = inTransformToTrack;
            _locality = inLocality;
        }

        public Transform getTransform() {
            return _transformToTrack;
        }

        public XMathTypes.TransformData getTransformData() {
            return new XMathTypes.TransformData(_transformToTrack, _locality);
        }

        public override bool hasValueChanges() {
            XMathTypes.TransformData theNextValue = getNextValue();
            return !XMath.equalsWithPrecision(_value, theNextValue);
        }

        public override bool applyChanges() {
            bool theHasValueChanges = hasValueChanges();
            _value = getNextValue();
            return theHasValueChanges;
        }

        public override bool revertChanges() {
            bool theHadValueChanges = hasValueChanges();
            _value.applyForTransform(_transformToTrack, _locality);
            return theHadValueChanges;
        }

        private XMathTypes.TransformData getNextValue() {
            return new XMathTypes.TransformData(_transformToTrack, _locality);
        }

        [HideInInspector] public Transform _transformToTrack = null;
        [HideInInspector] public XMathTypes.Locality _locality = XMathTypes.Locality.Local;

        [HideInInspector] public XMathTypes.TransformData _value;
    }
}
