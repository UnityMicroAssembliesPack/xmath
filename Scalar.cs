﻿using UnityEngine;

public partial class XMath
{
    public enum RoundingType {
        Floor,
        Round,
        Ceil
    }

    //TODO: Move to Vectors
    public enum VectorDimention {
        X,
        Y,
        Z
    }

    public static Optional<float> protectedDivision(float inValue, float inDivider) {
        return (0.0f == inDivider ? new Optional<float>() : new Optional<float>(inValue / inDivider));
    }

    public static int roundToInt(float inValue, RoundingType inRoundingType) {
        switch (inRoundingType) {
            case RoundingType.Floor:  return Mathf.FloorToInt(inValue);
            case RoundingType.Round:  return Mathf.RoundToInt(inValue);
            case RoundingType.Ceil:   return Mathf.CeilToInt(inValue);
            default:                  XUtils.check(false); return 0;
        }
    }

    public static int safeCast(uint inValue) {
        //TODO: Add check if some numbers are loosed
        return (int)inValue;
    }

    public static uint safeCast(int inValue) {
        //TODO: Add check if some numbers are loosed
        return (uint)inValue;
    }

    public static bool equalsWithPrecision(
        float inFloatA, float inFloatB,
        float inEpsilon = XMath.epsilon)
    {
        return Mathf.Abs(inFloatA - inFloatB) <= inEpsilon;
    }

    public static bool hasSameSigns(float inValueA, float inValueB) {
        return inValueA * inValueB >= 0.0f;
    }

    public static float getSign(float inValue) {
        return (inValue > 0.0f ? 1.0f : (inValue < 0.0f ? -1.0f : 0.0f));
    }

    public static int getSign(int inValue) {
        return (inValue > 0 ? 1 : (inValue < 0 ? -1 : 0));
    }

    public static uint max(uint inA, uint inB) {
        return inA > inB ? inA : inB;
    }

    public static float getValueRatioInRange(
        float inMinimum, float inMaximum, float inValue,
        float inMinimumRange = XMath.epsilon)
    {
        //TODO: Test minimum/maximum orientation?
        float theRange = inMaximum - inMinimum;
        return equalsWithPrecision(theRange, 0.0f, inMinimumRange) ? 0.0f : inValue / theRange;
    }

    public static void mergeToRange(ref int inoutStartA, ref uint inoutSizeA, int inStartB, uint inSizeB) {
        int theNewStart = Mathf.Min(inoutStartA, inStartB);
        uint theNewStartRelatedSizeA = safeCast(inoutStartA + safeCast(inoutSizeA) - theNewStart);
        uint theNewStartRelatedSizeB = safeCast(inStartB + safeCast(inSizeB) - theNewStart);

        inoutStartA = theNewStart;
        inoutSizeA = max(theNewStartRelatedSizeA, theNewStartRelatedSizeB);
    }

    //- - - Some operations

    public static uint log2(uint inValue) {
        XUtils.check(0 != inValue && 0 == inValue%2);

        uint theResult = 0;
        while (1 != inValue) {
            inValue = inValue >> 1;
            ++theResult;
        }
        return theResult;
    }

    // ===============================================
    //TODO: Move somewhere?

    public static float getRayDistanceForPoint(Ray inRay, Vector3 inPoint, bool inCheckPointOnRay = false) {
        Vector3 theProjection = Vector3.Project(inPoint - inRay.origin, inRay.direction);
        float theProjectionMagnitude = theProjection.magnitude;
        float theAngle = getAngleBetween(inRay.direction, theProjection);

        const float kEpsilon = 0.5f;
        if (inCheckPointOnRay) {
            XUtils.check(Mathf.Abs(theAngle) % 180.0f < kEpsilon);
        }

        int theDistanceSign = Mathf.Abs(theAngle) < 90.0f ? 1 : -1;
        return theProjectionMagnitude * theDistanceSign;
    }

    // ========== From 2D Project (method to possible integrate them) ==========

    ////Vector actions
    public static Vector3 getDiractionVectorForAngle(float inAngle) {
        float theAngleRadians = inAngle * Mathf.Deg2Rad;
        return new Vector3(Mathf.Cos(theAngleRadians), Mathf.Sin(theAngleRadians), 0.0f);
    }
    //
    //public static void getFromTransform(
    //    ref Vector2 outWorldPosition, ref float outWorldRotation,
    //    Transform inTransform)
    //{
    //    outWorldPosition = inTransform.position;
    //
    //    Quaternion theLocalRotation = Quaternion.Euler(
    //        0.0f, 0.0f, inTransform.rotation.eulerAngles.z
    //    );
    //
    //    Transform theParentTransform = inTransform.gameObject.transform.parent;
    //    Quaternion theWorldRotation = theParentTransform ?
    //        theLocalRotation : theParentTransform.rotation * theLocalRotation;
    //
    //    outWorldRotation = theWorldRotation.eulerAngles.z;
    //}
    //
    //public static void getFromTransform(
    //    ref Vector2 outWorldPosition, ref float outWorldRotatioin,
    //    GameObject inObjectWithTransform)
    //{
    //    getFromTransform(ref outWorldPosition, ref outWorldRotatioin,
    //        inObjectWithTransform.transform
    //    );
    //}
    //
    //public static Vector3 getVectorRotatedBy90Degrees(Vector3 inVector) {
    //    return new Vector3(-inVector.y, inVector.x, 0.0f);
    //}
    //
    //public static Vector2 rotate(Vector2 inVector, float inDegrees) {
    //    float theDegrees = inDegrees * Mathf.Deg2Rad;
    //    float theSin = Mathf.Sin(theDegrees);
    //    float theCos = Mathf.Cos(theDegrees);
    //
    //    return new Vector2(
    //        (theCos * inVector.x) - (theSin * inVector.y),
    //        (theSin * inVector.x) + (theCos * inVector.y)
    //    );
    //}
    //
    ////Numberic
    //public static bool equalsWithPrecision(float inValueA, float inValueB, float inPrecision) {
    //    return Mathf.Abs(inValueA - inValueB) <= inPrecision;
    //}
    //
    //public static bool hasSameSigns(float inValueA, float inValueB) {
    //    return inValueA * inValueB >= 0.0f;
    //}
    //
    //public static float getValueRatioInRange(
    //    float inMinimum, float inMaximum, float inValue, float inMinimumRange = 0.001f)
    //{
    //    float theRange = inMaximum - inMinimum;
    //    return equalsWithPrecision(theRange, 0.0f, inMinimumRange) ? 0.0f : inValue / theRange;
    //}
    //
    ////Angles
    //public static float getNearestAngleBetweenPoints(Vector2 inPointFrom, Vector2 inPointTo) {
    //    Vector2 theDelta = inPointTo - inPointFrom;
    //    return Mathf.Atan2(theDelta.y, theDelta.x) * Mathf.Rad2Deg;
    //}
    //
    //public static float getNormalizedAngle(float inAngle) {
    //    float theAngle = inAngle % 360.0f;
    //    return theAngle > 180.0f ? theAngle - 360.0f :
    //        theAngle < -180.0f ? theAngle + 360.0f : theAngle;
    //}
    //
    //public static float getNormalizedAnglesClockwiseDelta(float inFrom, float inTo) {
    //    float theDelta = inTo - inFrom;
    //    if (theDelta < 0.0f) theDelta += 360;
    //    return theDelta;
    //}
    //
    ////Transform
    //public static void assignTransform(Transform outTo, Transform inFrom) {
    //    outTo.position = inFrom.position;
    //    outTo.rotation = inFrom.rotation;
    //    outTo.localScale = inFrom.localScale;
    //}
}
