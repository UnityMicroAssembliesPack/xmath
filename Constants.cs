﻿public partial class XMath
{
    public const float epsilon = 0.001f;

    // ================================================================
    // TODO: Move to appropriate place!!!
    // ================================================================

    public static float getNormalizedAngle(float inAngle) {
        float theAngle = inAngle % 360.0f;
        return theAngle > 180.0f ? theAngle - 360.0f :
            theAngle < -180.0f ? theAngle + 360.0f : theAngle;
    }

    public static float getNonperiodicAngle(float inNormalizedAngle) {
        return inNormalizedAngle + 180.0f;
    }

    public static void normalizeAngle(ref float inAngle) {
        inAngle = getNormalizedAngle(inAngle);
    }

    public static float getNormalizedAnglesClockwiseDelta(float inFrom, float inTo) {
        float theDelta = inTo - inFrom;
        if (theDelta < 0.0f) theDelta += 360;
        return theDelta;
    }
}
