﻿using UnityEngine;

namespace XMathTypes
{
    public enum Locality {
        None,
        Local,
        Global
    }

    public enum TransformPlane {
        None,
        XY,
        XZ,
        YZ
    }

    // -------------------- Transform data ------------------------

    [System.Serializable]
    public struct TransformData
    {
        public TransformData(Transform inTransform, Locality inLocality) {
            if (!inTransform) {
                position = Vector3.zero;
                rotation = Quaternion.identity;
                scale = Vector3.one;
                return;
            }

            switch (inLocality) {
                case Locality.Local:
                    position = inTransform.position;
                    rotation = inTransform.localRotation;
                    scale = inTransform.localScale;
                    break;

                case Locality.Global:
                    position = inTransform.position;
                    rotation = inTransform.rotation;
                    scale = inTransform.lossyScale;
                    break;

                default:
                    XUtils.check(false);
                    position = Vector3.zero;
                    rotation = Quaternion.identity;
                    scale = Vector3.one;
                    break;
            }
        }

        //NB: Scale is not applied globaly
        public void applyForTransform(Transform inTransform, Locality inLocality) {
            switch (inLocality) {
                case Locality.Local:
                    inTransform.localPosition = position;
                    inTransform.localRotation = rotation;
                    inTransform.localScale = scale;
                    break;

                case Locality.Global:
                    inTransform.position = position;
                    inTransform.rotation = rotation;
                    inTransform.localScale = scale;
                    break;
            }
        }

        public override string ToString() {
            string theResult = "Transform {\n";
            theResult += "\tPosition: " + position.ToString() + "\n";
            theResult += "\tRotation: " + rotation.eulerAngles.ToString() + "\n";
            theResult += "\tScale: " + scale.ToString() + "\n";
            theResult += "}";
            return theResult;
        }

        //TODO: Implement
        public TransformData getWorldTransformData(Transform inParentTransform) {
            XUtils.check(false, "NOT IMPLEMENTED");
            return new TransformData();
        }

        public Vector3 axisX {
            get { return rotation * Vector3.right; }
        }

        public Vector3 axisY {
            get { return rotation * Vector3.up; }
        }

        public Vector3 axisZ {
            get { return rotation * Vector3.forward; }
        }

        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;
    }

    // -------------------- Locator data ------------------------

    [System.Serializable]
    public struct LocatorTransform
    {
        public LocatorTransform(Vector3 inPosition) {
            position = inPosition;
            rotation = Quaternion.identity;
        }

        public LocatorTransform(Vector3 inPosition, Quaternion inRotation) {
            position = inPosition;
            rotation = inRotation;
        }

        public LocatorTransform(TransformData inTransformData) {
            position = inTransformData.position;
            rotation = inTransformData.rotation;
        }

        public LocatorTransform(Transform inTransform, Locality inLocality = Locality.Global) {
            position = Vector3.zero;
            rotation = Quaternion.identity;
            if (!inTransform) return;

            switch (inLocality) {
                case Locality.Global:
                    position = inTransform.position;
                    rotation = inTransform.rotation;
                    break;

                case Locality.Local:
                    position = inTransform.localScale;
                    rotation = inTransform.localRotation;
                    break;

                case Locality.None:
                    XUtils.check(false);
                    break;
            }
        }

        public LocatorTransform(Vector3 inOriginPoint, Vector3 inNormal, Vector3 inXAxisOrientation) {
            position = inOriginPoint;

            Vector3 theForward = Vector3.Cross(inXAxisOrientation, inNormal);
            rotation = Quaternion.LookRotation(theForward, inNormal);
        }

        public Vector3 getGlobalPoint(Vector3 inLocalPoint) {
            return rotation * inLocalPoint + position;
        }

        public Vector3 getLocalPoint(Vector3 inGlobalPoint) {
            return Quaternion.Inverse(rotation) * (inGlobalPoint - position);
        }

        public Vector3 getGlobalDiraction(Vector3 inLocalDiraction) {
            return rotation * inLocalDiraction;
        }

        public Vector3 getLocalDiraction(Vector3 inGlobalDiraction) {
            return Quaternion.Inverse(rotation) * inGlobalDiraction;
        }

        public Quaternion getGlobalRotation(Quaternion inLocalRotation) {
            return rotation * inLocalRotation;
        }

        public Quaternion getLocalRotation(Quaternion inGlobalRotation) {
            return Quaternion.Inverse(rotation) * inGlobalRotation;
        }

        public LocatorTransform getGlobalTransform(LocatorTransform inLocalTransform) {
            return new LocatorTransform(
                getGlobalPoint(inLocalTransform.position),
                getGlobalRotation(inLocalTransform.rotation)
            );
        }

        public LocatorTransform getLocalTransform(LocatorTransform inGlobalTransform) {
            return new LocatorTransform(
                getLocalPoint(inGlobalTransform.position),
                getLocalRotation(inGlobalTransform.rotation)
            );
        }

        public void rotate(Quaternion inRotation) {
            rotation *= inRotation;
        }

        public void rotate(XMath.VectorDimention inAxis, float inAngle) {
            switch(inAxis) {
                case XMath.VectorDimention.X:  rotation *= Quaternion.Euler(inAngle, 0.0f, 0.0f);  break;
                case XMath.VectorDimention.Y:  rotation *= Quaternion.Euler(0.0f, inAngle, 0.0f);  break;
                case XMath.VectorDimention.Z:  rotation *= Quaternion.Euler(0.0f, 0.0f, inAngle);  break;
            }
        }

        public LocatorTransform rotated(Quaternion inRotation) {
            LocatorTransform theTransform = this;
            theTransform.rotate(inRotation);
            return theTransform;
        }

        public LocatorTransform rotated(XMath.VectorDimention inAxis, float inAngle) {
            LocatorTransform theTransform = this;
            theTransform.rotate(inAxis, inAngle);
            return theTransform;
        }

        public Plane getPlane(TransformPlane inPlane) {
            switch (inPlane) {
                case TransformPlane.XY:   return new Plane(axisZ, position);
                case TransformPlane.XZ:   return new Plane(axisY, position);
                case TransformPlane.YZ:   return new Plane(axisX, position);
            }
            XUtils.check(false);
            return new Plane();
        }

        public Vector3 axisX {
            get { return rotation * Vector3.right; }
        }

        public Vector3 axisY {
            get { return rotation * Vector3.up; }
        }

        public Vector3 axisZ {
            get { return rotation * Vector3.forward; }
        }

        public void applyForTransform(Transform inTransform, Locality inLocality = Locality.Global) {
            switch (inLocality) {
                case Locality.Global:
                    inTransform.position = position;
                    inTransform.rotation = rotation;
                    return;

                case Locality.Local:
                    inTransform.localPosition = position;
                    inTransform.localRotation = rotation;
                    return;
            }
            XUtils.check(false);
        }

        public static LocatorTransform lerp(LocatorTransform inA, LocatorTransform inB, float inParam) {
            return new LocatorTransform(
                Vector3.Lerp(inA.position, inB.position, inParam),
                Quaternion.Slerp(inA.rotation, inB.rotation, inParam)
            );
        }

        public static LocatorTransform identity { get { return new LocatorTransform(Vector3.zero); } }

        public Vector3 position;
        public Quaternion rotation;
    }
}

// =================== XMath transform actions ======================

public partial class XMath
{
    public static void resetTransform(Transform inTransform, XMathTypes.Locality inLocality = XMathTypes.Locality.Local) {
        switch (inLocality) {
            case XMathTypes.Locality.Local:
                inTransform.localPosition = Vector3.zero;
                inTransform.localRotation = Quaternion.identity;
                inTransform.localScale = Vector3.one;
                break;

            case XMathTypes.Locality.Global:
                inTransform.position = Vector3.zero;
                inTransform.rotation = Quaternion.identity;
                //inTransform.localScale = Vector3.one;  <<< Scale not supported
                break;

            default:
                XUtils.check(false);
                break;
        }
    }

    public static void assignTransform(Transform outTo, Transform inFrom) {
        outTo.position = inFrom.position;
        outTo.rotation = inFrom.rotation;
        outTo.localScale = inFrom.localScale;
    }

    static public XMathTypes.LocatorTransform getGlobalTransform(
        Transform inParentTransform, XMathTypes.LocatorTransform inLocalTransform)
    {
        if (!inParentTransform) return inLocalTransform;

        return new XMathTypes.LocatorTransform(
            inParentTransform.TransformPoint(inLocalTransform.position),
            inParentTransform.rotation * inLocalTransform.rotation
        );
    }

    static public XMathTypes.LocatorTransform getGlobalTransform(
        XMathTypes.LocatorTransform inParentTransform, XMathTypes.LocatorTransform inLocalTransform)
    {
        return new XMathTypes.LocatorTransform(
            inParentTransform.getGlobalPoint(inLocalTransform.position),
            inParentTransform.getGlobalRotation(inLocalTransform.rotation)
        );
    }

    static public XMathTypes.LocatorTransform getLocalTransform(
        XMathTypes.LocatorTransform inParentTransform, XMathTypes.LocatorTransform inGlobalTransform)
    {
        return new XMathTypes.LocatorTransform(
            inParentTransform.getLocalPoint(inGlobalTransform.position),
            inParentTransform.getLocalRotation(inGlobalTransform.rotation)
        );
    }
    
    //TODO: Make possible to setup precision
    public static bool equalsWithPrecision(XMathTypes.LocatorTransform inA, XMathTypes.LocatorTransform inB)
    {
        if (!equalsWithPrecision(inA.position, inB.position, 0.0f)) return false;
        if (!equalsWithPrecision(inA.rotation, inB.rotation, 0.0f)) return false;

        return true;
    }

    //TODO: Make possible to setup precision
    public static bool equalsWithPrecision(
        XMathTypes.TransformData inA, XMathTypes.TransformData inB)
    {
        if (!equalsWithPrecision(inA.position, inB.position, 0.0f)) return false;
        if (!equalsWithPrecision(inA.rotation, inB.rotation, 0.0f)) return false;
        if (!equalsWithPrecision(inA.scale, inB.scale, 0.0f)) return false;

        return true;
    }
}
