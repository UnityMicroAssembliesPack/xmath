﻿using UnityEngine;
using XMathTypes;

public partial class XMath
{
    public static void getPitchAndYawFromDiraction(out float outYaw, out float outPitch, Vector3 inDiraction) {
        outYaw = -Mathf.Atan2(inDiraction.z, inDiraction.x) * Mathf.Rad2Deg;

        float theXZLength = getVector2From(inDiraction, TransformPlane.XZ).magnitude;
        outPitch = Mathf.Atan2(inDiraction.y, theXZLength) * Mathf.Rad2Deg;
    }

    // --- Per component actions ---

    public static void perComponentMultiply(ref Vector3 inoutVector, Vector3 inMultiplyer) {
        inoutVector.x *= inMultiplyer.x;
        inoutVector.y *= inMultiplyer.y;
        inoutVector.z *= inMultiplyer.z;
    }

    public static void perComponentDivide(ref Vector2 inoutVector, Vector2 inDivider) {
        inoutVector.x /= inDivider.x;
        inoutVector.y /= inDivider.y;
    }

    public static void perComponentDivide(ref Vector3 inoutVector, Vector3 inDivider) {
        inoutVector.x /= inDivider.x;
        inoutVector.y /= inDivider.y;
        inoutVector.z /= inDivider.z;
    }

    public static Vector2 perComponentDivision(Vector2 inVector, Vector2 inDivider) {
        Vector2 theResult = inVector;
        perComponentDivide(ref theResult, inDivider);
        return theResult;
    }

    public static Vector3 perComponentMultiplication(Vector3 inVector, Vector3 inMultiplier) {
        Vector3 theResult = inVector;
        perComponentMultiply(ref theResult, inMultiplier);
        return theResult;
    }

    public static Vector3 perComponentDivision(Vector3 inVector, Vector3 inDivider) {
        Vector3 theResult = inVector;
        perComponentDivide(ref theResult, inDivider);
        return theResult;
    }

    public static ProtectedVector3 perComponentProtectedDivision(Vector3 inVector, Vector3 inDivider) {
        ProtectedVector3 theResult;
        theResult.x = protectedDivision(inVector.x, inDivider.x);
        theResult.y = protectedDivision(inVector.y, inDivider.y);
        theResult.z = protectedDivision(inVector.z, inDivider.z);
        return theResult;
    }

    // --- Infinity vector ---

    public static Vector2 getInifinityVector2() {
        return new Vector3(float.MaxValue, float.MaxValue);
    }

    public static Vector3 getInifinityVector3() {
        return new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
    }

    public static bool isInfinityVector(Vector2 inVector) {
        return float.MaxValue == inVector.x &&
            float.MaxValue == inVector.y;
    }

    public static bool isInfinityVector(Vector3 inVector) {
        return float.MaxValue == inVector.x &&
            float.MaxValue == inVector.y &&
            float.MaxValue == inVector.z;
    }

    // --- Transform plane access ---

    public static Plane getTransformPlane(Transform inTransform, TransformPlane inPlane) {
        return getTransformPlane(inTransform, inPlane, Vector3.zero);
    }

    public static Plane getTransformPlane(Transform inTransform, TransformPlane inPlane, Vector3 inPointOnPlane) {
        XUtils.check(TransformPlane.None != inPlane);

        //TODO: Implement for other
        switch (inPlane) {
            case TransformPlane.XZ:         return new Plane(inTransform.up, inPointOnPlane);
            default: XUtils.check(false);   return new Plane();
        }
    }

    // --- 2D <-> 3D vector transforms ---

    public static Vector3 getVector3From(Vector2 inVector2,TransformPlane inTransformPlane = TransformPlane.XZ,
        float inDimensionDefaultValue = 0.0f)
    {
        switch (inTransformPlane) {
            case TransformPlane.XY:        return new Vector3(inVector2.x, inVector2.y, inDimensionDefaultValue);
            case TransformPlane.XZ:        return new Vector3(inVector2.x, inDimensionDefaultValue, inVector2.y);
            case TransformPlane.YZ:        return new Vector3(inDimensionDefaultValue, inVector2.x, inVector2.y);
            default: XUtils.check(false);  return new Vector3();
        }
    }

    public static Vector2 getVector2From(Vector3 inVector3, TransformPlane inTransformPlane = TransformPlane.XZ) {
        switch (inTransformPlane) {
            case TransformPlane.XY:       return new Vector2(inVector3.x, inVector3.y);
            case TransformPlane.XZ:       return new Vector2(inVector3.x, inVector3.z);
            case TransformPlane.YZ:       return new Vector2(inVector3.y, inVector3.z);
            default: XUtils.check(false); return new Vector2();
        }
    }

    public static IntVector3 getVector3From(IntVector2 inVector2, TransformPlane inTransformPlane = TransformPlane.XZ,
        int inDimensionDefaultValue = 0)
    {
        switch (inTransformPlane) {
            case TransformPlane.XY:        return new IntVector3(inVector2.x, inVector2.y, inDimensionDefaultValue);
            case TransformPlane.XZ:        return new IntVector3(inVector2.x, inDimensionDefaultValue, inVector2.y);
            case TransformPlane.YZ:        return new IntVector3(inDimensionDefaultValue, inVector2.x, inVector2.y);
            default: XUtils.check(false);  return new IntVector3();
        }
    }

    public static IntVector2 getVector2From(IntVector3 inVector3, TransformPlane inTransformPlane = TransformPlane.XZ) {
        switch (inTransformPlane) {
            case TransformPlane.XY:       return new IntVector2(inVector3.x, inVector3.y);
            case TransformPlane.XZ:       return new IntVector2(inVector3.x, inVector3.z);
            case TransformPlane.YZ:       return new IntVector2(inVector3.y, inVector3.z);
            default: XUtils.check(false); return new IntVector2();
        }
    }

    // --- Misc ---

    static public Vector2 project(Vector2 inVector, Vector2 inNormal) {
        return Vector2.Dot(inNormal, inVector) * inNormal / inNormal.sqrMagnitude;
    }

    static public Vector2 getVectorRotatedByHalfPINum(Vector2 inVector, int inHalfPINum) {
        switch (inHalfPINum % 4) {
            case 0:           return inVector;
            case 1: case -3:  return new Vector2(inVector.y, -inVector.x);
            case 2: case -2:  return -inVector;
            case 3: case -1:  return new Vector2(-inVector.y, inVector.x);
            default:  XUtils.check(false); return new Vector2();
        }
    }

    static public IntVector2 getVectorRotatedByHalfPINum(IntVector2 inVector, int inHalfPINum) {
        switch (inHalfPINum % 4) {
            case 0:           return inVector;
            case 1: case -3:  return new IntVector2(inVector.y, -inVector.x);
            case 2: case -2:  return -inVector;
            case 3: case -1:  return new IntVector2(-inVector.y, inVector.x);
            default:  XUtils.check(false); return new IntVector2();
        }
    }

    static public IntVector3 getVectorRotatedByHalfPINum(IntVector3 inVector, VectorDimention inAxis, int inHalfPINum){
        if (inVector.isZero()) return inVector;

        switch (inAxis) {
            case VectorDimention.X:
                return getVector3From(
                    getVectorRotatedByHalfPINum(getVector2From(inVector, TransformPlane.YZ), inHalfPINum),
                    TransformPlane.YZ, inVector.x
                );

            case VectorDimention.Y:
                return getVector3From(
                    getVectorRotatedByHalfPINum(getVector2From(inVector, TransformPlane.XZ), inHalfPINum),
                    TransformPlane.XZ, inVector.y
                );

            case VectorDimention.Z:
                return getVector3From(
                    getVectorRotatedByHalfPINum(getVector2From(inVector, TransformPlane.XY), inHalfPINum),
                    TransformPlane.XY, inVector.z
                );

            default:
                XUtils.check(false); return new IntVector3();
        }
    }

    static public IntVector3 getVectorRotatedByHalfPINum(IntVector3 inVector, IntVector3 inHalfPIRotations) {
        if (inVector.isZero()) return inVector;

        IntVector3 theResult = inVector;
        theResult = getVectorRotatedByHalfPINum(theResult, VectorDimention.Y, inHalfPIRotations.y);
        theResult = getVectorRotatedByHalfPINum(theResult, VectorDimention.Z, inHalfPIRotations.z);
        theResult = getVectorRotatedByHalfPINum(theResult, VectorDimention.X, inHalfPIRotations.x);
        return theResult;
    }

    static public bool isValid(Quaternion inQuaternion) {
        return !isQuaternionHasNAN(inQuaternion) && !isZeroQuaternion(inQuaternion);
    }

    static private bool isZeroQuaternion(Quaternion inQuaternion) {
        if (0.0f != inQuaternion.x) return false;
        if (0.0f != inQuaternion.y) return false;
        if (0.0f != inQuaternion.z) return false;
        if (0.0f != inQuaternion.w) return false;
        return true;
    }

    static private bool isQuaternionHasNAN(Quaternion inQuaternion) {
        if (float.IsNaN(inQuaternion.x)) return true;
        if (float.IsNaN(inQuaternion.y)) return true;
        if (float.IsNaN(inQuaternion.z)) return true;
        if (float.IsNaN(inQuaternion.w)) return true;
        return false;
    }

    public static float getAngleBetween(Vector2 inVectorA, Vector2 inVectorB) {
        bool theAreClockwiseOriented = areVectorsClockwiseOriented(inVectorA, inVectorB);
        float theSign = theAreClockwiseOriented ? 1.0f : -1.0f;
        return theSign * Vector2.Angle(inVectorA, inVectorB);
    }

    public static float getAngleBetween(Vector3 inVectorA, Vector3 inVectorB) {
        bool theAreClockwiseOriented = areVectorsClockwiseOriented(inVectorA, inVectorB);
        float theSign = theAreClockwiseOriented ? 1.0f : -1.0f;
        return theSign * Vector3.Angle(inVectorA, inVectorB);
    }

    public static bool equalsWithPrecision(Vector2 inVectorA, Vector2 inVectorB, float inEpsilon = XMath.epsilon) {
        if (!equalsWithPrecision(inVectorA.x, inVectorB.x, inEpsilon)) return false;
        if (!equalsWithPrecision(inVectorA.y, inVectorB.y, inEpsilon)) return false;
        return true;
    }

    public static bool equalsWithPrecision(Vector3 inVectorA, Vector3 inVectorB, float inEpsilon = XMath.epsilon) {
        if (!equalsWithPrecision(inVectorA.x, inVectorB.x, inEpsilon)) return false;
        if (!equalsWithPrecision(inVectorA.y, inVectorB.y, inEpsilon)) return false;
        if (!equalsWithPrecision(inVectorA.z, inVectorB.z, inEpsilon)) return false;
        return true;
    }

    public static bool equalsWithPrecision(
        Quaternion inQuaternionA, Quaternion inQuaternionB, float inEpsilon = XMath.epsilon)
    {
        if (!equalsWithPrecision(inQuaternionA.x, inQuaternionB.x, inEpsilon)) return false;
        if (!equalsWithPrecision(inQuaternionA.y, inQuaternionB.y, inEpsilon)) return false;
        if (!equalsWithPrecision(inQuaternionA.z, inQuaternionB.z, inEpsilon)) return false;
        if (!equalsWithPrecision(inQuaternionA.w, inQuaternionB.w, inEpsilon)) return false;
        return true;
    }

    public static float getCrossProductLength(Vector2 inA, Vector2 inB) {
        return inA.x*inB.y - inA.y*inB.x;
    }

    public static float getCrossProductLength(Vector3 inA, Vector3 inB) {
        return (inA.y * inB.z - inA.z * inB.y) - (inA.x * inB.z - inA.z * inB.x) + (inA.x * inB.y - inA.y * inB.x);
    }

    public static bool areVectorsCooriented(Vector3 inA, Vector3 inB, float inEpsilon = XMath.epsilon) {
        Vector3 theANormalized = inA.normalized;
        Vector3 theBNormalized = inB.normalized;
        return equalsWithPrecision(inA, inB, inEpsilon) || equalsWithPrecision(inA, -inB, inEpsilon);
    }

    public static bool isVectorInsideVector(Vector2 inVector, Vector2 inVectorToTest) {
        float theVectorLength = inVector.magnitude;
        float theVectorToTestLength = inVectorToTest.magnitude;
        return equalsWithPrecision(inVector / theVectorLength, inVectorToTest / theVectorToTestLength) &&
            (theVectorLength >= theVectorToTestLength);
    }

    public static bool areVectorsClockwiseOriented(Vector2 inA, Vector2 inB) {
        return getCrossProductLength(inA, inB) <= 0.0f;
    }

    public static bool areVectorsClockwiseOriented(Vector3 inA, Vector3 inB) {
        return getCrossProductLength(inA, inB) <= 0.0f;
    }

    public static Vector3 getDiractionFromRotation(Quaternion inQuaternion) {
        return inQuaternion * Vector3.right;
    }

    public static Quaternion getRotationFromDirection(Vector3 inDirection) {
        return getRotationFromDirection(inDirection, Vector3.up);
    }

    public static Quaternion getRotationFromDirection(Vector3 inDirection, Vector3 inUp) {
        Vector3 theZ = Vector3.Cross(inDirection, inUp);
        return Quaternion.LookRotation(theZ, inUp);
    }
}
